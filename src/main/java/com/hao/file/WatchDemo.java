package com.hao.file;

import com.google.common.base.Charsets;
import com.sun.nio.file.SensitivityWatchEventModifier;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

import static com.sun.jmx.mbeanserver.Util.cast;

public class WatchDemo {
    public static void main(String[] args) throws IOException, InterruptedException {
        //监听目录(项目resource/config目录)，即运行时classes/config目录
//        String baseDir = Thread.currentThread().getContextClassLoader().getResource("config").getPath();
        File directory = new File("");
        String baseDir = directory.getAbsolutePath();
        System.out.println(baseDir);
        //监听文件
        String target_file = "t.txt";
        //构造监听服务
        WatchService watcher = FileSystems.getDefault().newWatchService();
        //监听注册，监听实体的创建、修改、删除事件，并以高频率(每隔2秒一次，默认是10秒)监听
        Paths.get(baseDir).register(watcher,
                new WatchEvent.Kind[]{StandardWatchEventKinds.ENTRY_CREATE,
                        StandardWatchEventKinds.ENTRY_MODIFY,
                        StandardWatchEventKinds.ENTRY_DELETE},
                SensitivityWatchEventModifier.HIGH);
        while (true) {
            //每隔3秒拉取监听key
            WatchKey key = watcher.poll(3, TimeUnit.SECONDS);  //等待，超时就返回
            //监听key为null,则跳过
            if (key == null) {
                continue;
            }
            //获取监听事件
            for (WatchEvent<?> event : key.pollEvents()) {
                //获取监听事件类型
                WatchEvent.Kind kind = event.kind();
                //异常事件跳过
                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }
                //获取监听Path
                Path path = cast(event.context());
                //只关注目标文件
                if (!target_file.equals(path.toString())) {
                    continue;
                }
                //文件删除
                if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
                    System.out.printf("file delete, type:%s  path:%s \n", kind.name(), path);
                    continue;
                }
                if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
                    System.out.println("file modify ");
                }
                //构造完整路径
                Path fullPath = Paths.get(baseDir, path.toString());
                //获取文件
//                File f = fullPath.toFile();
                //读取文件内容
                String content = new String(Files.readAllBytes(fullPath), Charsets.UTF_8);
                //按行读取文件内容
//                List<String> lineList = Files.readAllLines(fullPath);
                //输出事件类型、文件路径及内容
                System.out.printf("type:%s  path:%s  content:%s\n", kind.name(), path, content);
            }
            //处理监听key后(即处理监听事件后)，监听key需要复位，便于下次监听
            key.reset();
        }
    }
}
