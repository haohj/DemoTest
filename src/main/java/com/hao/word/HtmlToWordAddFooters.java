package com.hao.word;

import com.aspose.words.*;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

public class HtmlToWordAddFooters {
    private static final String fileName = "未命名";
    static FileSystemView fsv = FileSystemView.getFileSystemView();
    static File com = fsv.getHomeDirectory();
    private static final String dataDir = com + File.separator + fileName + ".doc";

    public static void main(String[] args) throws Exception {
        Document doc = new Document();
        DocumentBuilder builder = new DocumentBuilder(doc);
        builder.moveToHeaderFooter(HeaderFooterType.FOOTER_PRIMARY);
        builder.write("第 ");
        builder.insertField("PAGE", "");
        builder.write(" 页 ");
        builder.getCurrentParagraph().getParagraphFormat().setAlignment(ParagraphAlignment.CENTER);
        //builder.moveToDocumentEnd();
        doc.save(dataDir, SaveOptions.createSaveOptions(SaveFormat.DOC));
    }

    /**
     * Clones and copies headers/footers form the previous section to the specified section.
     */
    private static void copyHeadersFootersFromPreviousSection(Section section) throws Exception {
        Section previousSection = (Section) section.getPreviousSibling();

        if (previousSection == null) {
            return;
        }

        section.getHeadersFooters().clear();

        for (HeaderFooter headerFooter : previousSection.getHeadersFooters()) {
            section.getHeadersFooters().add(headerFooter.deepClone(true));
        }
    }
}
