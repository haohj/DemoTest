package com.hao.word;

import com.aspose.words.*;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

public class CreateCatlog {
    static FileSystemView fsv = FileSystemView.getFileSystemView();
    static File com = fsv.getHomeDirectory();
    private static final String path = com + File.separator;

    public static void main(String[] args) throws Exception {

        //ExStart:InsertATableOfContentsUsingHeadingStyles
        Document doc = new Document();

        // Create a document builder to insert content with into document.
        DocumentBuilder builder = new DocumentBuilder(doc);

       doc.getFirstSection().getBody().prependChild(new Paragraph(doc));

       builder.moveToDocumentStart();

        builder.insertTableOfContents("\\o \"1-3\" \\h \\z \\u");

       builder.insertBreak(BreakType.SECTION_BREAK_NEW_PAGE);

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_1);

        builder.writeln("Heading 1");

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_2);

        builder.writeln("Heading 1.1");
        builder.writeln("Heading 1.2");

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_1);

        builder.writeln("Heading 2");
        builder.writeln("Heading 3");

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_2);

        builder.writeln("Heading 3.1");

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_3);

        builder.writeln("Heading 3.1.1");
        builder.writeln("Heading 3.1.2");
        builder.writeln("Heading 3.1.3");

        builder.getParagraphFormat().setStyleIdentifier(StyleIdentifier.HEADING_2);

        builder.writeln("Heading 3.2");
        builder.writeln("Heading 3.3");
        doc.updateFields();

        for (Paragraph para : (Iterable<Paragraph>) doc.getChildNodes(NodeType.PARAGRAPH, true)) {
            // Check if this paragraph is formatted using the TOC result based styles. This is any style between TOC and TOC9.
            if (para.getParagraphFormat().getStyle().getStyleIdentifier() >= StyleIdentifier.TOC_1 && para.getParagraphFormat().getStyle().getStyleIdentifier() <= StyleIdentifier.TOC_9) {
                System.out.println(para.getText());
                // Get the first tab used in this paragraph, this should be the tab used to align the page numbers.
                TabStop tab = para.getParagraphFormat().getTabStops().get(0);
                // Remove the old tab from the collection.
                para.getParagraphFormat().getTabStops().removeByPosition(tab.getPosition());
                // Insert a new tab using the same properties but at a modified position.
                // We could also change the separators used (dots) by passing a different Leader type
                para.getParagraphFormat().getTabStops().add(tab.getPosition() - 50, tab.getAlignment(), tab.getLeader());
            }
        }

        doc.save(path + "TableOfContentsTabStops_Out.doc");
    }
}
