package com.hao.word;

import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.StyleIdentifier;
import com.aspose.words.Underline;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;

public class CreateHyperlink {
    private static FileSystemView fsv = FileSystemView.getFileSystemView();
    private static File com = fsv.getHomeDirectory();
    private static final String PATH = com + File.separator;

    public static void main(String[] args) throws Exception {
        Document doc = new Document();
        DocumentBuilder builder = new DocumentBuilder(doc);

        builder.pushFont(); //If you want to pop it later

        builder.getFont().setColor(Color.BLUE);

        builder.getFont().setUnderline(Underline.NONE);


        builder.insertHyperlink("百度", "http://www.baidu.com", false);

        builder.popFont();
        builder.getFont().setStyleIdentifier(StyleIdentifier.HYPERLINK);
        builder.insertHyperlink("百度一下，你就知道", "http://www.baidu.com", false);


        doc.save(PATH + "TableOut.doc");
    }
}
