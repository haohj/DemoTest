package com.hao.service;

import javax.xml.ws.Endpoint;

public class Server {
    public static void main(String[] args) {
        System.out.println("web service start");
        HelloWorld implementor = new HelloWorldImpl();
        String address = "http://192.168.1.100/helloWorld";
        Endpoint.publish(address, implementor);  // JDK实现
        System.out.println("web service started");
    }
}
