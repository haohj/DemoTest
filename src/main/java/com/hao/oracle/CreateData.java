package com.hao.oracle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 自动制造测试数据
 */
public class CreateData {
    public static JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        //启动IoC容器
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        //获取IoC容器中JdbcTemplate实例
        jdbcTemplate = (JdbcTemplate) ctx.getBean("jdbcTemplate");

        //1.需要传入表名
        List<String> tableNames = new ArrayList<>();
        tableNames.add("bjxx");
        tableNames.add("SXPJWBL");
        tableNames.add("GJGX");
        tableNames.add("FJBSXPJSC");

        for (String tableName : tableNames) {
            //查询当前表中的数量，确定自增主键的当前位置
            List<Map<String, Object>> mapList = jdbcTemplate.queryForList("select * from " + tableName);
            String sql = "select t.COLUMN_NAME,a.DATA_TYPE,a.DATA_LENGTH,a.DATA_PRECISION,a.DATA_SCALE,a.NULLABLE,a.DATA_DEFAULT " +
                    "from user_col_comments t,user_tab_columns a " +
                    "where t.table_name=a.TABLE_NAME and t.column_name=a.COLUMN_NAME and t.table_name ='" + tableName.toUpperCase() + "'";
            List<Map<String, Object>> cols = jdbcTemplate.queryForList(sql);
            
            sql = "insert into " + tableName + " (aaa022,bdc301,aaa001,aaa002,aaa003)values ()";
            for (int i = 1; i <= 12; i++) {
                for (int j = 0; j < 12; j++) {

                    jdbcTemplate.update(sql);
                }
            }

        }
    }
}
