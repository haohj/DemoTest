package com.hao.client;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

public class client {
    public static void main(String[] args) {
        try {
            String endpoint = "http://122.224.25.203:7001/tzswws/wssb?wsdl";
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            call.setOperationName(new javax.xml.namespace.QName("com.serviceTargetName", "getWssbUserInfo"));
            call.addParameter("ticket", org.apache.axis.encoding.XMLType.XSD_DATE,
                    javax.xml.rpc.ParameterMode.IN);//接口的参数
            call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);//设置返回类型
            String temp = "{ticket:5153aaf38b5a4e2ab3f625dca412a5ea}";
            String result = (String)call.invoke(new Object[]{temp});
            //给方法传递参数，并且调用方法
            System.out.println("result is "+result);
        }
        catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
