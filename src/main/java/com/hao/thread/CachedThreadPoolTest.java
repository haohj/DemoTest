package com.hao.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author haohj
 * @date 2020-04-29 11:09
 */
public class CachedThreadPoolTest {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();
        System.out.println(service);

        /*for (int i = 0; i < 20; i++) {
            service.execute(() -> System.out.println(Thread.currentThread().getName()));
        }*/

        for (int i = 0; i < 2; i++) {
            service.execute(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
            });
        }
        System.out.println(service);
        TimeUnit.SECONDS.sleep(70);
        System.out.println(service);
        service.shutdown();
    }
}
