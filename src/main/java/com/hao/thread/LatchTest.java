package com.hao.thread;

import com.hao.utils.HttpClientOp;

import java.util.concurrent.CountDownLatch;

/**
 * @author haohj
 * @date 2020-04-23 13:49
 */
public class LatchTest {
    public static void main(String[] args) throws InterruptedException {
        Runnable taskTemp = new Runnable() {

            // 注意，此处是非线程安全的，留坑
            private int iCounter;

            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    // 发起请求
                    //HttpClientOp.doGet("https://www.baidu.com/");
                    String result = HttpClientOp.doPost("http://127.0.0.1:6010/control/interface/queryData", "param_filter=[%7B%22name%22:%22dq_name%22,%22value%22:%223301%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D,%7B%22name%22:%22cycle_name%22,%22value%22:%22202001%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D]&fkey=JYHJ&url=http:%2F%2F192.2.2.138%2Fapi%2FgetDataByForward&key=base_jyhj_czrks_tday&type_=flop&param_filter2=[%7B%22name%22:%22dq_name%22,%22value%22:%223301%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D,%7B%22name%22:%22cycle_name%22,%22value%22:%22202001%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D]&param_filter3=[%7B%22name%22:%22dq_name%22,%22value%22:%223301%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D,%7B%22name%22:%22cycle_name%22,%22value%22:%22202001%22,%22type%22:%22%3D%22,%22search_type%22:%22%22%7D]");
                    System.out.println(result);
                    iCounter++;
                    System.out.println(System.nanoTime() + " [" + Thread.currentThread().getName() + "] iCounter = " + iCounter);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        LatchTest latchTest = new LatchTest();
        latchTest.startTaskAllInOnce(1, taskTemp);
    }

    public long startTaskAllInOnce(int threadNums, final Runnable task) throws InterruptedException {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(threadNums);
        for (int i = 0; i < threadNums; i++) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        // 使线程在此等待，当开始门打开时，一起涌入门中
                        startGate.await();
                        try {
                            task.run();
                        } finally {
                            // 将结束门减1，减到0时，就可以开启结束门了
                            endGate.countDown();
                        }
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            };
            t.start();
        }
        long startTime = System.nanoTime();
        System.out.println(startTime + " [" + Thread.currentThread() + "] All thread is ready, concurrent going...");
        // 因开启门只需一个开关，所以立马就开启开始门
        startGate.countDown();
        // 等等结束门开启
        endGate.await();
        long endTime = System.nanoTime();
        System.out.println(endTime + " [" + Thread.currentThread() + "] All thread is completed.");
        return endTime - startTime;
    }
}
