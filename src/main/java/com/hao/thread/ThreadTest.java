package com.hao.thread;

/**
 * 通过继承Thread类实现多线程
 * Thread类是有run()方法的
 * 也有start方法
 *
 * @author haohj
 * @date 2020-04-28 16:46
 */
public class ThreadTest extends Thread {
    private int ticket = 10;
    private String name;

    public ThreadTest(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        //super.run();
        while (true) {
            if (this.ticket > 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(this.name + "卖票--->" + (this.ticket--));
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        ThreadTest th1 = new ThreadTest("一号窗口");
        ThreadTest th2 = new ThreadTest("二号窗口");
        th1.start();
        th2.start();
    }
}
