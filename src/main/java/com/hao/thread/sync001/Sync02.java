package com.hao.thread.sync001;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author haohj
 * @date 2020-04-28 11:24
 */
public class Sync02 {
    public static class Lock {
    }

    public static void main(String[] args) {
        Lock lock = new Lock();
        System.out.println(ClassLayout.parseInstance(lock).toPrintable());
        System.out.println("##########################################################");

        synchronized (lock) {
            System.out.println(ClassLayout.parseInstance(lock).toPrintable());
        }
    }
}
