package com.hao.thread;

/**
 * 通过实现Runnable接口，实现多线程
 * Runnable类是有run()方法的
 * 但是没有start方法
 *
 * @author haohj
 * @date 2020-04-28 16:56
 */
public class RunnableTest implements Runnable {
    private int ticket = 10;

    @Override
    public void run() {
        while (true) {
            if (this.ticket > 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "卖票--->" + (this.ticket--));
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        RunnableTest mt = new RunnableTest();
        Thread t1 = new Thread(mt, "一号窗口");
        Thread t2 = new Thread(mt, "二号窗口");
        t1.start();
        t2.start();
    }
}
