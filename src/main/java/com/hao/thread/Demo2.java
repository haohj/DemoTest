package com.hao.thread;

import java.util.stream.IntStream;

/**
 * @program: DemoTest->Demo2
 * @description:
 * @author: haohj
 * @create: 2019-07-19 16:06
 **/
public class Demo2 {
    public static void main(String[] args) {
        IntStream.range(0, 5).boxed()
                .map(i -> new Thread(() -> System.out.println(Thread.currentThread().getName())))
                .forEach(Thread::start);
    }
}
