package com.hao.thread;

/**
 * @author haohj
 * @date 2019-12-24 11:12
 */
public class MutiThread {
    private int num = 0;

    public synchronized void method1(String tag) {
        num = 100;
        System.out.println("set num 100");
        System.out.println("tag :" + tag + ",num :" + num);
    }

    public synchronized void method2(String tag) {
        num = 200;
        System.out.println("set num 200");
        System.out.println("tag :" + tag + ",num :" + num);
    }

    public static void main(String[] args) {
        MutiThread m = new MutiThread();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                m.method1("a");
            }
        };

        Thread t2 = new Thread(){
            @Override
            public void run() {
                m.method2("b");
            }
        };


        t1.start();
        t2.start();
    }
}
