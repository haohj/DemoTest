package com.hao.entity;


import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.util.Date;

/**
 * 导出导入实体对象
 * <p>
 * 导出注解说明
 * columnName:导出Excel列名
 * scale:导出BigDecimal类型格式化精度
 * roundingMode:导出BigDecimal类型舍入规则
 * dateFormat:导出Data类型格式化模式
 * defaultCellValue:导出模板默认值
 * <p>
 * 导入注解说明
 * required:是否非空校验
 * regex:正则校验规则
 * regexMessage:正则校验失败信息
 * scale:导出BigDecimal类型格式化精度
 * roundingMode:导出BigDecimal类型舍入规则
 * dateFormat:导出Data类型格式化模式
 */
@ExcelTarget("table")
public class Table implements java.io.Serializable{
    /**
     * 学生姓名
     */
    @Excel(name = "名称", orderNum = "1", needMerge = true)
    private String colnum;
    /**学生性别*/
    @Excel(name = "类型")
    private String type;


    @Excel(name = "可为空")
    private String ableNull;
    @Excel(name = "默认")
    private String defaultValue;
    @Excel(name = "主键")
    private String primaryKey;
    @Excel(name = "注释")
    private String comment;

    public String getColnum() {
        return colnum;
    }

    public void setColnum(String colnum) {
        this.colnum = colnum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAbleNull() {
        return ableNull;
    }

    public void setAbleNull(String ableNull) {
        this.ableNull = ableNull;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Table{" +
                "colnum='" + colnum + '\'' +
                ", type='" + type + '\'' +
                ", ableNull='" + ableNull + '\'' +
                ", defaultValue='" + defaultValue + '\'' +
                ", primaryKey='" + primaryKey + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
