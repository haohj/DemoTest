package com.hao.entity;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

@ExcelTarget("aa01")
public class Aa01 implements java.io.Serializable{
    @Excel(name = "单位名称")
    private String aa0101;
    @Excel(name = "姓名")
    private String name;
    @Excel(name = "身份证号码")
    private String idCare;
}
