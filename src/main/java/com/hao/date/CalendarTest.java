package com.hao.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author haohj
 * @date 2020-01-02 15:41
 */
public class CalendarTest {
    public static void main(String[] args) {
        /*Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));*/

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formatDate = format.format(new Date());
        System.out.println(formatDate);
        String year = formatDate.substring(0,4);
        System.out.println(year);
        System.out.println(formatDate.substring(5,7));
        System.out.println(formatDate.substring(8,10));
    }
}
