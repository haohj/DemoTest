package com.hao.date;

import com.hao.utils.ChineseDateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @description: 根据日期获取星期几
 * @author: haohj
 * @create: 2019-07-30 14:48
 **/
public class DateTest1 {

    public static void main(String[] args) {
        String dateStr = "2019-07-30 14:00:00";
        //注意：SimpleDateFormat构造函数的样式与strDate的样式必须相符
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //必须捕获异常
        try {
            Date date = simpleDateFormat.parse(dateStr);
            System.out.println(getWeek(date));
            System.out.println(ChineseDateUtil.getCNDate(date));
            System.out.println(ChineseDateUtil.getCNYear(date));
            System.out.println(ChineseDateUtil.getCNMonth(date));
            System.out.println(ChineseDateUtil.getCNDay(date));
        } catch (Exception px) {
            px.printStackTrace();
        }

        System.out.println(getWeek2(new Date()));
    }

    public static String getWeek(Date date) {
        String[] weeks = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return weeks[week_index];
    }

    public static String getWeek2(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String week = sdf.format(date);
        return week;
    }
}
