package com.hao.ssh;

import com.jcraft.jsch.*;

import java.io.InputStream;
import java.util.concurrent.*;

public class Demo2 {
    public static void main(String[] args) {
        /*ExecutorService executorService = new ThreadPoolExecutor(20, 50,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());*/

        for (int j = 0; j < 10000; j++) {
//            long currentTimeMillis = System.currentTimeMillis();

            String command = "/home/hzrsapp/workspaces/sqoop help ";

            JSch jsch = new JSch();
            Session session = null;
            try {
                session = jsch.getSession("hzrsapp", "172.16.83.11", 22);
                session.setPassword("HZrs_appjy082");
                session.setConfig("StrictHostKeyChecking", "no");
                session.connect(60 * 1000);
                ChannelExec channel = (ChannelExec) session.openChannel("exec");
                channel.setCommand(command);

                channel.setInputStream(null);

                channel.setErrStream(System.err);

                InputStream in = channel.getExtInputStream();
                channel.connect();

                /*byte[] tmp = new byte[1024];
                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, 1024);
                        if (i < 0) {
                            break;
                        }
                        System.out.print(new String(tmp, 0, i));
                    }
                    if (channel.isClosed()) {
                        if (in.available() > 0) {
                            continue;
                        }
                        System.out.println("exit-status: " + channel.getExitStatus());
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ee) {
                    }
                }*/
                channel.disconnect();
            } catch (Exception e) {
                System.out.println("命令执行失败");
                e.printStackTrace();
            } finally {
                session.disconnect();
            }

//            long currentTimeMillis1 = System.currentTimeMillis();
//            System.out.println("Jsch方式" + (currentTimeMillis1 - currentTimeMillis));
            System.out.println("Jsch方式" + j);
        }
    }
}
