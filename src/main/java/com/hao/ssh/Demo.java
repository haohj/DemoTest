package com.hao.ssh;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

import java.io.*;

public class Demo {
    public static void main(String[] args) {
        String hostname = "192.168.140.128";
//        String hostname = "172.16.83.11";
        String username = "root";
//        String username = "hzrsapp";
        String password = "94hhj0607";
//        String password = "HZrs_appjy082";

        Connection conn = new Connection(hostname);
        try {
            conn.connect();
            boolean isAuthenticated = conn.authenticateWithPassword(username, password);
            if (isAuthenticated) {
                Session sess = conn.openSession();
//                sess.execCommand("/home/hzrsapp/workspaces/sqoop list-databases --connect jdbc:mysql://localhost:3306/ --username root --password nysql ");
                sess.execCommand("python /home/haohj/datax/bin/datax.py /home/haohj/datax/bin/oracle2es.json ");

                String result = processStdout(sess.getStdout(), "UTF-8");
                System.out.println(result);

                sess.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    }

    private static String processStdout(InputStream in, String charset) {
        InputStream stdout = new StreamGobbler(in);
        StringBuffer buffer = new StringBuffer();
        ;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout, charset));
            String line = null;
            while ((line = br.readLine()) != null) {
                buffer.append(line + "\n");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }
}
