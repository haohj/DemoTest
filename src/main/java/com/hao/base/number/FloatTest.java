package com.hao.base.number;

public class FloatTest {
    public static void main(String[] args) {
        System.out.println("0 => " + isNumber("0"));
        System.out.println("0.1 => " + isNumber("0.1"));
        System.out.println("abc => " + isNumber("abc"));
        System.out.println("1 a => " + isNumber("1 a"));
        System.out.println("2e10 => " + isNumber("2e10"));
        System.out.println(" -90e3    => " + isNumber(" -90e3   "));
        System.out.println("959440.94f => " + isNumber("959440.94f"));
    }

    private static boolean isNumber(String s) {
        try {
            Float.parseFloat(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
