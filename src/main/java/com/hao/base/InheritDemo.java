package com.hao.base;

/**
 * @description:
 * @author: haohj
 * @create: 2019-07-22 08:46
 **/
public class InheritDemo {
    public static void main(String[] args) {
        Son son = new Son();
        son.print();
        son.speak();
    }
}
