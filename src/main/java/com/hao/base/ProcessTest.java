package com.hao.base;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * 杀进程测试类
 */
public class ProcessTest {
    public static void main(String[] args) {
        try {
//            String cmdStr4 = "cmd /c C:\\Users\\haohj\\Desktop\\haohj.docx";
            String cmdStr4 = "cmd /c python C:\\Users\\haohj\\Desktop\\sum.py";
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(cmdStr4);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.forName("GBK")));
            String line = null;
            String s = "";
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                s += line + "\n";
            }
            br.close();
            System.out.println(s);


//            process.waitFor();
            InputStream inputStream = process.getInputStream();
            Scanner input = new Scanner(inputStream);
            String result = "";
            while (input.hasNextLine()) {
                result += input.nextLine() + "\n";
            }
            System.out.println(result);
            /** 休眠5秒后关闭子进程树*/
            Thread.sleep(15000);
            SystemUtils.killProcessTree(process);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
