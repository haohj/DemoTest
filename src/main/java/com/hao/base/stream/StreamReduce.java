package com.hao.base.stream;

import java.util.stream.Stream;

/**
 * @author haohj
 * @date 2020-04-14 13:51
 */
public class StreamReduce {
    public static void main(String[] args) {
        reduceTest();
    }

    public static void reduceTest() {
        //累加，初始化值是 10
        Integer reduce = Stream.of(1, 2, 3, 4, 5)
                .reduce(0, (count, item) -> {
                    System.out.println("count:" + count);
                    System.out.println("item:" + item);
                    return count + item;
                });
        Integer result = Stream.of(1, 2, 3, 4, 5).reduce(0, (count, item) -> item + count);
        System.out.println(result);
        System.out.println(reduce);

        Integer reduce1 = Stream.of(1, 2, 3, 4)
                .reduce(0, (x, y) -> x + y);
        System.out.println(reduce1);

        String reduce2 = Stream.of("1", "2", "3")
                .reduce("0", (x, y) -> (x + "," + y));
        System.out.println(reduce2);
    }
}
