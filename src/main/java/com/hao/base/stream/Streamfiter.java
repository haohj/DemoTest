package com.hao.base.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author haohj
 * @date 2020-04-14 09:01
 */
public class Streamfiter {
    /**
     * 过滤所有的男性
     */
    public static void fiterSex() {
        List<PersonModel> list = Data.getData();

        //用原先的老方法，遍历比较过滤
        List<PersonModel> temp = new ArrayList<>();
        for (PersonModel personModel : list) {
            if ("男".equals(personModel.getSex())) {
                temp.add(personModel);
            }
        }
        System.out.println(temp);

        //使用stream流操作
        List<PersonModel> collect = list.stream().filter(personModel -> "男".equals(personModel.getSex())).collect(Collectors.toList());
        System.out.println(collect);
    }

    /**
     * 过滤所有的男性 并且小于20岁
     */
    public static void fiterSexAndAge() {
        List<PersonModel> list = Data.getData();

        //用原先的老方法，遍历比较过滤
        List<PersonModel> temp = new ArrayList<>();
        for (PersonModel personModel : list) {
            if ("男".equals(personModel.getSex()) && (personModel.getAge() < 20)) {
                temp.add(personModel);
            }
        }
        System.out.println(temp);

        //使用stream流操作
//        List<PersonModel> collect = list.stream().filter(personModel -> "男".equals(personModel.getSex())).filter(personModel -> personModel.getAge() < 20).collect(Collectors.toList());
//        List<PersonModel> collect = list.stream().filter(personModel -> ("男".equals(personModel.getSex()) && personModel.getAge() < 20)).collect(Collectors.toList());
        List<PersonModel> collect = list.stream().filter(personModel -> {
                    if ("男".equals(personModel.getSex()) && personModel.getAge() < 20) {
                        return true;
                    }
                    return false;
                }
        ).collect(Collectors.toList());
        System.out.println(collect);
    }

    public static void main(String[] args) {
        fiterSexAndAge();
    }
}
