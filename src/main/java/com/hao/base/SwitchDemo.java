package com.hao.base;

public class SwitchDemo {
    public static void main(String[] args) {
        String s = "2";
        switch (s){
            case "1":
            case "2":
            case "3":
                System.out.println("3");
                break;
            case "4":
                System.out.println("4");
                break;
        }
    }
}
