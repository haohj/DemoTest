package com.hao.base;

/**
 * The type Son.
 *
 * @description:
 * @author: haohj
 * @create: 2019 -07-22 08:49
 */
public class Son extends Father implements SpeakEnglish{
    private String name;

    static {
        System.out.println("子类静态代码块");
    }

    {
        System.out.println("子类非静态代码块");
    }

    /**
     * Instantiates a new Son.
     *
     * @param name the name
     */
    public Son(String name) {
        this.name = name;
        System.out.println("子类有参构造");
    }

    /**
     * Instantiates a new Son.
     */
    public Son() {
        System.out.println("子类无参构造");
    }

    @Override
    public void print() {
        System.out.println("子类print方法");
    }

    @Override
    public void speak() {
        System.out.println("接口方法");
    }
}
