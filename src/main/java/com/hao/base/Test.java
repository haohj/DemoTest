package com.hao.base;

public class Test {

    public static int test() {
        int result = 0;
        try {
            throw new Exception();
        } catch (Exception e) {
            return ++result;
        } finally {
            result += 2;
            return result;
        }
    }
    public static void main(String[] args) {
        System.out.println("result = " + test());
    }
}
