package com.hao.base;

/**
 * @description: 反射测试
 * @author: haohj
 * @create: 2019-09-16 10:29
 **/
public class ClassTest {
    public static void main(String[] args) {
        try {
            Father father =  Son.class.newInstance();
            father.print();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
