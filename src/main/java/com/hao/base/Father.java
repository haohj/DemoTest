package com.hao.base;

/**
 * @description:
 * @author: haohj
 * @create: 2019-07-22 08:49
 **/
public class Father {
    private String name;

    static {
        System.out.println("父类静态代码块");
    }

    {
        System.out.println("父类非静态代码块");
    }

    public Father(String name) {
        this.name = name;
        System.out.println("父类有参构造，参数name=" + name);
    }

    public Father() {
        System.out.println("父类无参构造");
    }

    public void print(){
        System.out.println("父类print方法");
    }
}
