package com.hao.sort;

/**
 * 选择排序
 * <p>
 * a、将第一个值看成最小值
 * <p>
 * b、然后和后续的比较找出最小值和下标
 * <p>
 * c、交换本次遍历的起始值和最小值
 * <p>
 * d、说明：每次遍历的时候，将前面找出的最小值，看成一个有序的列表，后面的看成无序的列表，然后每次遍历无序列表找出最小值。
 *
 * @author haohj
 * @date 2020-07-13 15:27
 */
public class SelectionSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
        print(arr);
        sort(arr);
    }

    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int min = arr[i];
            int index = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    index = j;
                }
            }
            arr[index] = arr[i];
            arr[i] = min;
            print(arr);
        }
    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
