package com.hao.sort;

/**
 * 希尔排序
 * <p>
 * a、基本上和插入排序一样的道理
 * <p>
 * b、不一样的地方在于，每次循环的步长，通过减半的方式来实现
 * <p>
 * c、说明：基本原理和插入排序类似，不一样的地方在于。通过间隔多个数据来进行插入排序。
 *
 * @author haohj
 * @date 2020-07-13 15:29
 */
public class ShellSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
//        int arr[] = {8, 5, 3, 2, 4, 1, 6, 0, 7, 9};
        print(arr);
        sort(arr);
    }

    public static void sort(int[] arr) {
        for (int i = arr.length / 2; i > 0; i /= 2) {
            for (int j = i; j < arr.length; j++) {
                for (int k = j; k > 0 && k - i >= 0; k -= i) {
                    if (arr[k] < arr[k - i]) {
                        int temp = arr[k - i];
                        arr[k - i] = arr[k];
                        arr[k] = temp;
                    }
                }
            }
            print(arr);
        }
    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
