package com.hao.sort;

/**
 * 归并排序
 *
 * @author haohj
 * @date 2020-07-13 15:30
 */
public class MergeSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
        print(arr);
        sort(arr);
    }

    public static void sort(int[] arr) {

    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
