package com.hao.sort;

/**
 * 快速排序
 *
 * @author haohj
 * @date 2020-07-13 15:28
 */
public class QuickSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
        print(arr);
        sort(arr, 0, arr.length);
    }

    public static void sort(int[] arr, int l, int r) {
        if(l>=r){
            return;
        }

    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
