package com.hao.sort;

/**
 * 插入排序
 * <p>
 * a、默认从第二个数据开始比较。
 * <p>
 * b、如果第二个数据比第一个小，则交换。然后在用第三个数据比较，如果比前面小，则插入（狡猾）。否则，退出循环
 * <p>
 * c、说明：默认将第一数据看成有序列表，后面无序的列表循环每一个数据，如果比前面的数据小则插入（交换）。否则退出。
 *
 * @author haohj
 * @date 2020-07-13 15:28
 */
public class InsertionSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
        print(arr);
        sort(arr);
    }

    public static void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                } else {
                    break;
                }
                print(arr);
            }
        }
    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
