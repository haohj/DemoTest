package com.hao.sort;

/**
 * 冒泡排序
 * <p>
 * a、冒泡排序，是通过每一次遍历获取最大/最小值
 * <p>
 * b、将最大值/最小值放在尾部/头部
 * <p>
 * c、然后除开最大值/最小值，剩下的数据在进行遍历获取最大/最小值
 *
 * @author haohj
 * @date 2020-07-13 15:21
 */
public class BubbleSort {
    public static void main(String[] args) {
        int arr[] = {8, 5, 3, 2, 4};
        print(arr);
        sort(arr);
    }

    //冒泡排序
    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                //两两比较，小的放前面大的放后面
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
                print(arr);
            }
        }
    }

    public static void print(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + " ,");
            }
        }
        System.out.println("]");
    }
}
