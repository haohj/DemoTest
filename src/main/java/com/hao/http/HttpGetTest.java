package com.hao.http;

import cn.hutool.http.HttpUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class HttpGetTest {
    public static void main(String[] args) {
        ExecutorService service = new ThreadPoolExecutor(1000, 1000, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());

        String[] ports = {"1111", "1122", "2222", "2233", "3333", "3344", "4444", "4455", "5555", "5566", "6666", "6677", "7777", "7788", "8888", "8899", "1212", "2323"};
        for (String port : ports) {
            for (int i = 0; i < 40; i++) {
                service.submit(() -> {
                    String url = "http://192.168.140.128:" + port + "/";
                    System.out.println(url);
                    String result1 = HttpUtil.get(url);
                    System.out.println(result1);
                });
            }
        }
    }
}
