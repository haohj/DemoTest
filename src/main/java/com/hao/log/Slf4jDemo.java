package com.hao.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Slf4jDemo {
    private static Logger log = LoggerFactory.getLogger(Slf4jDemo.class);
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss SSS";
    public static void main(String[] args) {
        log.error("时间：{}，日志内容：{}",new SimpleDateFormat(DATETIME_FORMAT).format(new Date()),"哈哈哈哈");
    }
}
