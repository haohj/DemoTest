package com.hao.log;

import org.apache.log4j.Logger;

public class Log4jDemo {
    public static Logger log = Logger.getLogger(Log4jDemo.class);

    public static void main(String[] args) {
        log.info("我记录在Log4jDemo 文件中....");
        log.error("在Log4jDemo 文件中出现异常....");
    }
}
