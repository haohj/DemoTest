package com.hao.proxy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("springmvc.xml");
        HelloImpl helloImpl = (HelloImpl) context.getBean("greetingImpl");
        helloImpl.say("jack");
        Apology apology = (Apology) helloImpl;
        apology.saySorry("jack");
    }
}
