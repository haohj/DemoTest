package com.hao.proxy;

import org.springframework.aop.framework.ProxyFactory;

public class Client {
    public static void main(String[] args) {
        // 保存生成的代理类的字节码文件
        //System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        // jdk动态代理测试
        Subject subject = new JDKDynamicProxy(new RealSubject()).getProxy();
        subject.doSomething();

       /* CGLibProxy proxy = new CGLibProxy();
        Hello hello = proxy.getProxy(HelloImpl.class);*/

        /*Hello hello = CGLibProxy.getInstance().getProxy(HelloImpl.class);

        hello.say("Hercules");*/

        //创建代理工厂
        ProxyFactory proxyFactory = new ProxyFactory();
        //射入目标对象
        proxyFactory.setTarget(new HelloImpl());
        //添加前置增强
        //proxyFactory.addAdvice(new GreetingBeforeAdvice());
        //添加后置增强
        //proxyFactory.addAdvice(new GreetingAfterAdvice());

        //同时实现前置和后置增强
        //proxyFactory.addAdvice(new GreetingBeforeAndAfterAdvice());

        //添加环绕增强
        proxyFactory.addAdvice(new GreetingAroundAdvice());

        proxyFactory.addAdvice(new GreetingThrowAdvice());

        //从代理工厂获取代理
        Hello hello = (Hello) proxyFactory.getProxy();
        //调用代理方法
        hello.say("jack");
    }
}
