package com.hao.proxy;

public interface Subject {
    void doSomething();
}
