package com.hao.proxy;

public interface Apology {
    void saySorry(String name);
}
