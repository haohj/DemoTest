package com.hao.controller.websocket;

import com.hao.websocket.WebSocketTest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebSocketController {

    @RequestMapping(value="/webSocket1")
    public ModelAndView webSocket1() {
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("/WEB-INF/views/websocket/index.jsp");
        return modelView;
    }

    @RequestMapping(value="/webSocket2")
    public ModelAndView webSocket2() {
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("/WEB-INF/views/websocket/index2.jsp");
        WebSocketTest webSocketTest = new WebSocketTest();
        webSocketTest.sendToUser("hello|1234");
        return modelView;
    }

    @RequestMapping(value="/webSocket3")
    public ModelAndView webSocket3() {
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("/WEB-INF/views/websocket/index3.jsp");
        return modelView;
    }

    @RequestMapping(value="/test")
    public ModelAndView test() {
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("/WEB-INF/views/websocket/test.html");
        return modelView;
    }
}
