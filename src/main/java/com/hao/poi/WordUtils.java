package com.hao.poi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.poi.xwpf.converter.core.XWPFConverterException;

public class WordUtils {

    /**
     * <br>
     * 描 述: doc内容改变 <br>
     * 作 者: shizhenwei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     *            doc文档模板
     * @param params
     *            key value 将模板里的可以替换为响应VALUE
     * @return
     * @throws IOException
     */
    public static byte[] docContentChange(InputStream is, Map<String, String> params) throws IOException {
        return DocxUtils.docContentChange(is, params);
    }

    /**
     * <br>
     * 描 述: docx内容改变 <br>
     * 作 者: shizhenwei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     *            docx文档模板
     *            key value 将模板里的可以替换为响应VALUE
     * @return
     * @throws IOException
     * @throws XWPFConverterException
     */
    public static byte[] docxContentChange(InputStream is)
            throws XWPFConverterException, IOException {
        return DocxUtils.docxContentChange(is);
    }

    /***
     * <br>
     * 描 述: docx内容预览 <br>
     * 作 者: zhaowei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     * @return
     * @throws XWPFConverterException
     * @throws IOException
     */
    public static byte[] docxContentChangeView(InputStream is)
            throws XWPFConverterException, IOException {
        return DocxUtils.docxContentChange(is);
    }

    /***
     * <br>
     * 描 述: docx内容预览 <br>
     * 作 者: zhaowei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     * @return
     * @throws XWPFConverterException
     * @throws IOException
     */
    public static byte[] docxContentChangeDownload(InputStream is)
            throws XWPFConverterException, IOException {
        return DocxUtils.docxContentChange(is);
    }

    /**
     * <br>
     * 描 述: 将docx字节数组流转换为pdf字节数组流 <br>
     * 作 者: shizhenwei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param docxBytes
     *            docx文档字节数组
     * @return
     * @throws XWPFConverterException
     * @throws IOException
     *             注：需在部署系统安装word对应的中文字体
     */
    public static byte[] docx2pdf(byte[] docxBytes) throws XWPFConverterException, IOException {
        return DocxUtils.docx2pdf(docxBytes);
    }

    /**
     * <br>
     * 描 述: 将Word模板流改变内容后转换为pdf字节数组流 <br>
     * 作 者: shizhenwei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     *            docx文档输入流
     * @return
     * @throws IOException
     * @throws XWPFConverterException
     *             * 注：需在部署系统安装word对应的中文字体
     */
    public static byte[] docx2pdf(InputStream is)
            throws XWPFConverterException, IOException {
        return DocxUtils.docx2pdf(is);
    }
}