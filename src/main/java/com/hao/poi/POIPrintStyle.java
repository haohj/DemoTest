package com.hao.poi;

import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class POIPrintStyle {
    public static void main(String[] args) {
        //String path = "D:\\MyCode\\DemoTest\\src\\main\\resources\\exceltemplate\\jgunitmx.xls";
        String path = "D:\\MyCode\\rsglxt\\src\\main\\resources\\exceltemplate\\jgyhhz.xls";
        Workbook workbook = null;
        try {
            File file = new File(path);
            InputStream is = new FileInputStream(file);
            workbook = WorkbookFactory.create(is);
            Sheet sheet = workbook.getSheetAt(0);
            PrintSetup printSetup = sheet.getPrintSetup();
            System.out.println(printSetup.getScale());
        } catch (Exception e) {

        }

    }
}
