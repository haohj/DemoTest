package com.hao.poi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.xwpf.converter.core.XWPFConverterException;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
/**
 *
 * @author zhenwei.shi
 *
 */
public class DocxUtils {
    public static final String NULL_REPLACE = "   /   ";//如果没有替换的将显示"  "
    public static final String DOWNLOAD_REPLACE =  "      ";//下载替换word模板标签
    public static final String PREVIEW_REPLACE =  "   /   ";//预览替换word模板标签
    /**
     * <br>
     * 描 述: doc内容改变 <br>
     * 作 者: shizhenwei <br>
     * 历 史: (版本) 作者 时间 注释
     *
     * @param is
     *            doc文档模板
     * @param params
     *            key value 将模板里的可以替换为响应VALUE
     * @return
     * @throws IOException
     */
    public static byte[] docContentChange(InputStream is, Map<String, String> params) throws IOException {
        if(null==params){
            params = new HashMap<String,String>();
        }
        HWPFDocument document = new HWPFDocument(is);
        Range range = document.getRange();

        Set<String> keys = params.keySet();
        for (String key : keys) {
            range.replaceText(key.toString(), params.get(key));
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        document.write(baos);
        byte[] bytes = baos.toByteArray();

        baos.close();
        return bytes;
    }

    /**
     * <br>描 述:    改变word内容，讲word标签【*】替换为指定内容
     * <br>作 者: shizhenwei
     * <br>历 史: (版本) 作者 时间 注释
     * @return
     * @throws IOException
     * @throws XWPFConverterException
     */
    public static byte[] docxContentChange(InputStream is) throws XWPFConverterException, IOException{
        return docxContentChange(is);
    }

    /**
     * <br>描 述:    将docx字节数组流转换为pdf字节数组流
     * <br>作 者: shizhenwei
     * <br>历 史: (版本) 作者 时间 注释
     * @param docxBytes docx文档字节数组
     * @return
     * @throws XWPFConverterException
     * @throws IOException
     * 注：需在部署系统安装word对应的中文字体
     */
    public static byte[] docx2pdf(byte[] docxBytes) throws XWPFConverterException, IOException{
        PdfOptions options = PdfOptions.create();
        XWPFDocument document = new XWPFDocument(new ByteArrayInputStream(docxBytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfConverter.getInstance().convert(document, baos, options);
        return baos.toByteArray();
    }


    /**
     * <br>描 述:    将Word模板流改变内容后转换为pdf字节数组流
     * <br>作 者: shizhenwei
     * <br>历 史: (版本) 作者 时间 注释
     * @param is docx文档输入流
     * @return
     * @throws IOException
     * @throws XWPFConverterException
     * * 注：需在部署系统安装word对应的中文字体
     */
    public static byte[] docx2pdf(InputStream is) throws XWPFConverterException, IOException{
        XWPFDocument document = new XWPFDocument(is);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfOptions options = PdfOptions.create();
        PdfConverter.getInstance().convert(document, baos, options);
        byte[] bytes = baos.toByteArray();
        baos.close();
        return bytes;
    }

    //在替换的值左右延长两个空格长度
    public static String getReplaceValue(String old){
        return " "+old+" ";
    }

    /**
     * <br>描 述: 添加下划线
     * <br>作 者: shizhenwei
     * <br>历 史: (版本) 作者 时间 注释
     * @param run
     * @param underline
     * @return
     */
    public static XWPFRun addUnderline(XWPFRun run,boolean underline){
        //是否添加下划线
        if(underline){
            run.setUnderline(UnderlinePatterns.SINGLE);
        }else{
            run.setUnderline(UnderlinePatterns.NONE);
        }
        return run;
    }
}