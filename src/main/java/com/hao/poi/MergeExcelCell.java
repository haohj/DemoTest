package com.hao.poi;

import com.hao.utils.ExcelMergeCellUtils;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * @description: 合并单元格
 * @author: haohj
 * @create: 2019-07-24 10:36
 **/
public class MergeExcelCell {
    public static void main(String[] args) throws Exception {
        File file = new File("C:\\Users\\haohj\\Desktop\\招聘计划表 (3).xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = ExcelMergeCellUtils.getWorkbook(fileInputStream, "招聘计划表 (3).xlsx");
        ExcelMergeCellUtils.mergeCell(workbook, 0, 2, new int[]{0, 1});
        /*生成临时文件*/
        FileOutputStream out = null;
        String localPath = null;
        File tempFile = null;
        String fileName = String.valueOf(System.currentTimeMillis() / 1000);
        try {
            tempFile = File.createTempFile(fileName, ".xlsx");
            localPath = tempFile.getAbsolutePath();
            out = new FileOutputStream(localPath);
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(localPath);
    }


}
