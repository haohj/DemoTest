package com.hao.poi;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

import javax.swing.filechooser.FileSystemView;
import java.io.*;

public class WordToPDF {
    private static FileSystemView fsv = FileSystemView.getFileSystemView();
    private static File com = fsv.getHomeDirectory();
    private static final String PATH = com + File.separator;

    public static void main(String[] args) {
        String wordPath = PATH + "1.docx";
        String pdfPath = PATH + "1.pdf";
        try {
            InputStream bodyIs = new FileInputStream(wordPath);
            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            Font FontChinese = new Font(bfChinese, 12, Font.NORMAL);

            // 第一步，创建document对象
            Rectangle rectPageSize = new Rectangle(PageSize.A4);

            //下面代码设置页面横置
            //rectPageSize = rectPageSize.rotate();

            Document document = new Document();
            try {
                // 第二步,将Document实例和文件输出流用PdfWriter类绑定在一起
                //从而完成向Document写，即写入PDF文档
                PdfWriter.getInstance(document, new FileOutputStream("src/poi/itext/HelloWorld.pdf"));
                //第3步,打开文档
                document.open();


            } catch (DocumentException de) {
                System.err.println(de.getMessage());
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
            }
            //关闭document
            document.close();

            System.out.println("生成HelloPdf成功！");



            /*byte[] bytes = WordUtils.docx2pdf(bodyIs);
            OutputStream os = new FileOutputStream(pdfPath);
            os.write(bytes);
            os.close();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
