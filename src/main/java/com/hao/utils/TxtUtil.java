package com.hao.utils;


import java.io.*;

public class TxtUtil {

    private static String filenameTemp;
    private static final String[] parentFolders = {"财政接口", "机关接口", "离休接口", "事业接口"};
    private static final String[] jg = {"机关财政局", "机关湖州银行", "机关建设银行", "机关交行", "机关农行", "机关中国银行"};
    private static final String[] cz = {"机关财政局", "离休财政局", "事业财政局"};
    private static final String[] lx = {"离休财政局", "离休湖州银行", "离休建设银行", "离休交行", "离休农行", "离休中国银行"};
    private static final String[] sy = {"事业财政局", "事业建设银行", "事业交行", "事业农行（待定）", "事业中国银行"};

    public static void main(String[] args) throws IOException {
        TxtUtil.creatTxtFile("D:\\workspace\\DemoTest\\", "t");
        TxtUtil.writeTxtFile("你好");
        TxtUtil.writeTxtFile("的的");
        TxtUtil.writeTxtFile("de啊 ～～");
        TxtUtil.writeTxtFile("111\nddddddd");
    }


    /**
     * 创建文件
     *
     * @throws IOException
     */
    public synchronized static boolean creatTxtFile(String prepath, String name) throws IOException {
        boolean flag = false;
        filenameTemp = prepath + name + ".txt";
        System.out.println(filenameTemp);
        File filename = new File(filenameTemp);
        if (!filename.exists()) {
            filename.createNewFile();
            flag = true;
        }
        return flag;
    }

    /**
     * 写文件
     *
     * @param newStr 新内容
     * @throws IOException
     */
    public synchronized static boolean writeTxtFile(String newStr) throws IOException {
        // 先读取原有文件内容，然后进行写入操作
        boolean flag = false;
        String filein = newStr + "\r\n";
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            // 文件路径
            File file = new File(filenameTemp);
            // 将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();

            // 保存该文件原有的内容
            for (int j = 1; (temp = br.readLine()) != null; j++) {
                buf = buf.append(temp);
                // System.getProperty("line.separator")
                // 行与行之间的分隔符 相当于“\n”
                buf = buf.append(System.getProperty("line.separator"));
            }
            buf.append(filein);

            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buf.toString().toCharArray());
            pw.flush();
            flag = true;
        } catch (IOException e1) {
            throw e1;
        } finally {
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return flag;
    }

    public synchronized static void initDirectory(String bdc301) {
        String prepath = new Object() {
            public String getPath() {
                return this.getClass().getClassLoader().getResource("").getPath();
            }
        }.getPath();
        filenameTemp = prepath + bdc301 + "接口程序";
        File file = new File(filenameTemp);
        if (!file.exists()) {
            file.mkdir();
        }
        String temppath = null;
        for (String parentFolder : parentFolders) {
            temppath = filenameTemp + File.separator + bdc301 + parentFolder;
            file = new File(temppath);
            if (!file.exists()) {
                file.mkdir();
            }
            if (parentFolder.equals("机关接口")) {
                for (String s : jg) {
                    temppath = filenameTemp + File.separator + bdc301 + parentFolder + File.separator + s;
                    file = new File(temppath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                }
            }

            if (parentFolder.equals("事业接口")) {
                for (String s : sy) {
                    temppath = filenameTemp + File.separator + bdc301 + parentFolder + File.separator + s;
                    file = new File(temppath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                }
            }
            if (parentFolder.equals("财政接口")) {
                for (String s : cz) {
                    temppath = filenameTemp + File.separator + bdc301 + parentFolder + File.separator + s;
                    file = new File(temppath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                }
            }
            if (parentFolder.equals("离休接口")) {
                for (String s : lx) {
                    temppath = filenameTemp + File.separator + bdc301 + parentFolder + File.separator + s;
                    file = new File(temppath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                }
            }
        }
    }
}

