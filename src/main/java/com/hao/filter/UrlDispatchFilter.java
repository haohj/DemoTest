package com.hao.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

/**
 * @author haohj
 * @date 2020-01-10 16:22
 */
public class UrlDispatchFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String path = request.getRequestURI();
        if (path.indexOf("test") > -1) {
            String id = request.getParameter("id");
            if ("1".equals(id)) {
                request.getRequestDispatcher("/hello").forward(request, response);
            }
            if("2".equals(id)){
                request.getRequestDispatcher("/world").forward(request, response);
            }
        } else {
            filterChain.doFilter(request, response);
        }
        //response.sendRedirect("/hello");
    }

    @Override
    public void destroy() {

    }
}
