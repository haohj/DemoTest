package com.hao.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author haohj
 * @date 2020-06-28 10:24
 */
public class Demo13 {
    public static void main(String[] args) {
//        System.out.println(romanToInt("III"));//3
//        System.out.println(romanToInt("IV"));//4
//        System.out.println(romanToInt("IX"));//9
//        System.out.println(romanToInt("LVIII"));//58
        System.out.println(romanToInt("MCMXCIV"));//1994
    }

    public static int romanToInt(String s) {
        //I， V， X， L，C，D 和 M
        Map<String, Integer> map = new HashMap<>();
        map.put("I", 1);
        map.put("V", 5);
        map.put("X", 10);
        map.put("L", 50);
        map.put("C", 100);
        map.put("D", 500);
        map.put("M", 1000);
        map.put("IV", 4);
        map.put("IX", 9);
        map.put("XL", 40);
        map.put("XC", 90);
        map.put("CD", 400);
        map.put("CM", 900);
        if (s.length() == 1) {
            return map.get(s);
        }
        int result = 0;
        char[] arr = s.toCharArray();
        String s1 = "";
        for (int i = 1; i <= arr.length - 1; i++) {
            char lastch = arr[i - 1];
            char ch = arr[i];
            if (map.get(String.valueOf(lastch)) < map.get(String.valueOf(ch))) {
                s1 = String.valueOf(lastch) + String.valueOf(ch);
                result += map.get(s1);
            } else {
                if ("".equals(s1)) {
                    result += map.get(String.valueOf(lastch));
                } else {
                    s1 = "";
                }
                //result += map.get(String.valueOf(ch));
            }
        }
        if (map.get(String.valueOf(arr[arr.length - 1])) <= map.get(String.valueOf(arr[arr.length - 2]))) {
            result += map.get(String.valueOf(arr[arr.length - 1]));
        }
        return result;
    }

    public static int romanToInt2(String s) {

        return 0;
    }
}
