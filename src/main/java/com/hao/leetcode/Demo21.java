package com.hao.leetcode;

public class Demo21 {
    public static void main(String[] args) {
        Demo21 demo = new Demo21();
        // 输入：1->2->4, 1->3->4
        ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        //输出：1->1->2->3->4->4
        ListNode result = demo.mergeTwoLists(l1, l2);
        demo.print(result);

    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        if (l1.val <= l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l2.next, l1);
            return l2;
        }
    }

    public void print(ListNode listNode) {
        System.out.println(listNode.val);
        while (listNode.next != null) {
            listNode = listNode.next;
            System.out.println(listNode.val);
        }
    }
}
