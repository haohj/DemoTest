package com.hao.leetcode;

import java.util.Arrays;

public class Demo27 {
    public static void main(String[] args) {
        Demo27 demo = new Demo27();
        // 给定 nums = [3,2,2,3], val = 3,
        //函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
        int[] nums = new int[]{3, 2, 2, 3};
        int length = demo.removeElement2(nums, 3);
        System.out.println("调整后的数组长度为：" + length);
        demo.print(nums);
        // 给定 nums = [0,1,2,2,3,0,4,2], val = 2,
        //函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。
        nums = new int[]{0, 1, 2, 2, 3, 0, 4, 2};
        length = demo.removeElement2(nums, 2);
        System.out.println("调整后的数组长度为：" + length);
        demo.print(nums);
    }

    public int removeElement(int[] nums, int val) {
        //第一个指针，指向第一个匹配的元素下标
        int preIndex = 0;
        int lastIndex = nums.length - 1;
        while (preIndex < lastIndex) {
            int preValue = nums[preIndex];
            if (preValue == val) {
                preIndex++;
            } else {
                int lastValue = nums[lastIndex];
            }

        }
        return 0;
    }

    public int removeElement2(int[] nums, int val) {
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            }
        }
        return i;
    }

    public void print(int[] nums) {
        Arrays.stream(nums).forEach(num -> System.out.println(num));
    }
}
