package com.hao.leetcode;

/**
 * 给定一个Excel表格中的列名称，返回其相应的列序号
 * 实质上是一个26进制数转化为10进制
 */
public class Demo171 {
    public static void main(String[] args) {
        System.out.println(titleToNumber("AB"));
        System.out.println(titleToNumber("A"));
    }

    public static int titleToNumber(String s) {
        int ans = 0;
        for (int i = 0; i < s.length(); i++) {
            int num = s.charAt(i) - 'A' + 1;
            ans = ans * 26 + num;
        }
        return ans;
    }
}
