package com.hao.leetcode;

import java.util.Arrays;

public class Demo26 {
    public static void main(String[] args) {
        Demo26 demo = new Demo26();

        // 给定数组 nums = [1,1,2],
        //函数应该返回新的长度 2, 并且原数组 nums 的前两个元素被修改为 1, 2。
        int[] nums = new int[]{1, 1, 2};
        int length = demo.removeDuplicates(nums);
        System.out.println(length);
        demo.print(nums);
        System.out.println("##############################################");
        // 给定 nums = [0,0,1,1,1,2,2,3,3,4],
        //函数应该返回新的长度 5, 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4。
        nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        length = demo.removeDuplicates(nums);
        System.out.println(length);
        demo.print(nums);
    }

    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        //上一个元素的值
        int temp = nums[0];
        //上一个元素的下标
        int lastIndex = 0;
        for (int i = 1; i < nums.length; i++) {
            //如果当前元素和上个元素相等，则需要删除
            if (temp != nums[i]) {
                lastIndex++;
                nums[lastIndex] = nums[i];
            }
            temp = nums[i];
        }
        return lastIndex + 1;
    }

    public int removeDuplicates2(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int p = 0;
        int q = 1;
        while (q < nums.length) {
            if (nums[p] != nums[q]) {
                nums[p + 1] = nums[q];
                p++;
            }
            q++;
        }

        return p + 1;
    }

    public void print(int[] nums) {
        Arrays.stream(nums).forEach(num -> System.out.println(num));
    }
}
