package com.hao.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
 */
public class Demo22 {

    public static void main(String[] args) {
        Demo22 demo22 = new Demo22();
        List<String> list = demo22.generateParenthesis(3);
        for (String s : list) {
            System.out.println(s);
        }
    }

    public List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        dfs(n, n, "", list);
        return list;
    }

    private void dfs(int left, int right, String curStr, List<String> list) {
        System.out.println(left+"---"+right);
        if (left == 0 && right == 0) {
            list.add(curStr);
            return;
        }

        //((())) l:3-2-1 r:3-2-1
        if (left > 0) {
            dfs(left - 1, right, curStr + "(", list);
        }

        if (right > left) {
            dfs(left, right - 1, curStr + ")", list);
        }
    }
}
