package com.hao.docsToPdf;

import javax.swing.filechooser.FileSystemView;
import java.io.*;

public class DemoTest {
    private static FileSystemView fsv = FileSystemView.getFileSystemView();
    private static File com = fsv.getHomeDirectory();
    private static final String PATH = com + File.separator;

    public static void main(String[] args) throws Exception {
        Converter converter = null;
        File file = new File(PATH + "TestOut.pdf");
        File file2 = new File(PATH + "temp.doc");
        OutputStream outputStream = new FileOutputStream(file);
        InputStream inputStream = new FileInputStream(file2);

        if(!file.exists()){
            file.createNewFile();
        }

        converter = new DocToPDFConverter(inputStream, outputStream, true, true);
        converter.convert();
        //fileInputStream = new FileInputStream(file);
    }
}
