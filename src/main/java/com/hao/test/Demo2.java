package com.hao.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Demo2 {
    public static void main(String[] args) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        List<Map<String, Object>> result = new ArrayList<>();
        initData(mapList);
        //就业创业、社会保险、人事人才、劳动关系、其他
        String[] sort = {"就业创业", "社会保险", "人事人才", "劳动关系", "其他"};

        for (int i = 0; i < sort.length; i++) {
            for (Map<String, Object> m : mapList) {
                if (sort[i].equals(m.get("sxmc"))) {
                    result.add(m);
                }
            }
        }

        System.out.println(result.toString());

        for (Map<String, Object> m : mapList) {
            boolean flag = true;
            for (Map<String, Object> m2 : result) {
                if (m2.get("sxmc").equals(m.get("sxmc"))) {
                    flag = false;
                    continue;
                }
            }

            if (flag) {
                result.add(m);
            }
        }

        System.out.println(result.toString());
    }

    public static void initData(List<Map<String, Object>> mapList) {
        Map<String, Object> map1 = new HashMap<>();
        map1.put("sxmc", "就业创业");
        map1.put("sxlxid", "10");
        map1.put("wcbj", 46099);
        mapList.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("sxmc", "其他");
        map2.put("sxlxid", "14");
        map2.put("wcbj", 0);
        mapList.add(map2);

        Map<String, Object> map3 = new HashMap<>();
        map3.put("sxmc", "人事人才");
        map3.put("sxlxid", "12");
        map3.put("wcbj", 0);
        mapList.add(map3);

        Map<String, Object> map4 = new HashMap<>();
        map4.put("sxmc", "劳动关系");
        map4.put("sxlxid", "11");
        map4.put("wcbj", 0);
        mapList.add(map4);

        Map<String, Object> map5 = new HashMap<>();
        map5.put("sxmc", "社会保险");
        map5.put("sxlxid", "13");
        map5.put("wcbj", 2443);
        mapList.add(map5);

        Map<String, Object> map6 = new HashMap<>();
        map6.put("sxmc", "测试");
        map6.put("sxlxid", "13");
        map6.put("wcbj", 2443);
        mapList.add(map6);
    }
}
