package com.hao.easypoi;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.hao.utils.POIUtils;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class EasyPOIMapTest {
    public static void main(String[] args) {
        String[] name = {"hello", "world", "java"};
        String path = new EasyPOIMapTest().getClass().getClassLoader().getResource("").getPath() + "exceltemplate/test.xls";
        System.out.println(path);
        POIUtils.copySheet(path, 3);

        TemplateExportParams params = new TemplateExportParams("exceltemplate/test.xls");
        Integer[] index = {0, 1, 2};
        params.setSheetNum(index);
        params.setSheetName(name);
        Map<Integer, Map<String, Object>> result = new HashMap<>();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("sheetName", "hello");
        map1.put("sheetIndex", 0);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("sheetName", "world");
        map2.put("sheetIndex", 1);

        Map<String, Object> map3 = new HashMap<>();
        map3.put("sheetName", "java");
        map3.put("sheetIndex", 2);

        result.put(0, map1);
        result.put(1, map2);
        result.put(2, map3);
        Workbook workbook = ExcelExportUtil.exportExcel(result, params);
        try {
            FileOutputStream fos = new FileOutputStream("D:\\test.xls");
            workbook.write(fos);
            fos.close();
        } catch (Exception e) {

        }
    }
}
