package com.hao.easypoi;

import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EasyPOIExportTest {
    public static void main(String[] args) {
        List<EasyPOIModel> list = new ArrayList<>();
        int count1 = 0;
        EasyPOIModel easyPOIModel11 = new EasyPOIModel(String.valueOf(count1++), "信科", new User("张三", "男", 20));
        EasyPOIModel easyPOIModel12 = new EasyPOIModel(String.valueOf(count1++), "信科", new User("李四", "男", 17));
        EasyPOIModel easyPOIModel13 = new EasyPOIModel(String.valueOf(count1++), "信科", new User("淑芬", "女", 34));
        EasyPOIModel easyPOIModel14 = new EasyPOIModel(String.valueOf(count1++), "信科", new User("仲达", "男", 55));
        list.add(easyPOIModel11);
        list.add(easyPOIModel12);
        list.add(easyPOIModel13);
        list.add(easyPOIModel14);
        List<EasyPOIModel> list1 = new ArrayList<>();
        int count2 = 0;
        EasyPOIModel easyPOIModel21 = new EasyPOIModel(String.valueOf(count2++), "软件", new User("德林", "男", 22));
        EasyPOIModel easyPOIModel22 = new EasyPOIModel(String.valueOf(count2++), "软件", new User("智勇", "男", 28));
        EasyPOIModel easyPOIModel23 = new EasyPOIModel(String.valueOf(count2++), "软件", new User("廉贞", "女", 17));
        list1.add(easyPOIModel21);
        list1.add(easyPOIModel22);
        list1.add(easyPOIModel23);

        // 创建参数对象（用来设定excel得sheet得内容等信息）
        ExportParams params1 = new ExportParams();
        // 设置sheet得名称
        params1.setSheetName("表格1");
        params1.setTitle("2019年01月湖州市在职人员统发工资发放明细表");
        params1.setTitleHeight((short) 30);
        params1.setSecondTitle("测试");
        params1.setAddIndex(true);
        ;
        ExportParams params2 = new ExportParams();
        params2.setSheetName("表格2");
        params2.setTitle("2019年01月湖州市在职人员统发工资发放明细表");
        params2.setTitleHeight((short) 30);
        params2.setAddIndex(true);

        // 创建sheet1使用得map
        Map<String, Object> dataMap1 = new HashMap<>();
        // title的参数为ExportParams类型，目前仅仅在ExportParams中设置了sheetName
        dataMap1.put("title", params1);
        // 模版导出对应得实体类型
        dataMap1.put("entity", EasyPOIModel.class);
        // sheet中要填充得数据
        dataMap1.put("data", list);
        // 创建sheet2使用得map
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap2.put("title", params2);
        dataMap2.put("entity", EasyPOIModel.class);
        dataMap2.put("data", list1);
        // 将sheet1和sheet2使用得map进行包装
        List<Map<String, Object>> sheetsList = new ArrayList<>();
        sheetsList.add(dataMap1);
        sheetsList.add(dataMap2);
        // 执行方法
        Workbook workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
        try {
            FileOutputStream fos = new FileOutputStream("D:\\test.xls");
            workbook.write(fos);
            fos.close();
        } catch (Exception e) {

        }
    }
}
