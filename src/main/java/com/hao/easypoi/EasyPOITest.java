package com.hao.easypoi;

import com.hao.entity.Table;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;

import java.io.File;
import java.util.List;

public class EasyPOITest {
    public static void main(String[] args) {
        importExcelMap();
    }
    public static void importExcelMap(){
        ImportParams params = new ImportParams();
        params.setTitleRows(0);
        params.setHeadRows(1);
        params.setSheetNum(0);
        File file = new File("D:\\数据库说明文档.xls");
        List<Table> list = ExcelImportUtil.importExcel(file, Table.class, params);
        list.forEach(table -> System.out.println(table));
    }
}
