package com.hao.easypoi;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;


@ExcelTarget("easyPOIModel")
public class EasyPOIModel {
    // 属性
    @Excel(name = "序号", needMerge = true)
    private String index;

    @Excel(name = "班级", needMerge = true)
    private String className;

    @ExcelEntity(name = "用户信息")
    private User userInfo;


    // Setter&Getter
    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public User getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }

    // override-method
    @Override
    public String toString() {
        return "EasyPOIModel{" +
                "index='" + index + '\'' +
                ", className='" + className + '\'' +
                ", userInfo=" + userInfo +
                '}';
    }

    // Constructor
    public EasyPOIModel() {
    }

    public EasyPOIModel(String index, String className, User userInfo) {
        this.index = index;
        this.className = className;
        this.userInfo = userInfo;
    }
}
