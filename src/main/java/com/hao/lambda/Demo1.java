package com.hao.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo1 {
    public static void main(String[] args) {
        List<String> allArtists = new ArrayList<>();
        allArtists.add("London");
        allArtists.add("Los Angeles");
        allArtists.add("London");
        allArtists.add("Houston");
        allArtists.add("London");
        long count = allArtists.stream().filter(artist -> {
            System.out.println(artist);
            return artist.equals("London");
        }).count();
        System.out.println(count);

        List<String> collected = Stream.of("a","b","c").collect(Collectors.toList());
        String[] strarr = {"a","b","c"};
        System.out.println(Arrays.asList(strarr).equals(collected));
    }
}
