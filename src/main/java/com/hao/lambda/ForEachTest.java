package com.hao.lambda;

import java.util.ArrayList;
import java.util.List;

public class ForEachTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        list.forEach(x -> {
            if (x == 1) {
                return;
            }
            System.out.println(x);
        });

        System.out.println(list);
    }
}
