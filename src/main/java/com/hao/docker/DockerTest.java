package com.hao.docker;

import org.voovan.docker.DockerGlobal;
import org.voovan.docker.command.Image.CmdHubImageInfo;
import org.voovan.tools.json.JSON;
import org.voovan.tools.log.Logger;

public class DockerTest {
    public static void main(String[] args) {
        try {

            DockerGlobal.DOCKER_REST_HOST = "192.168.140.128";
            DockerGlobal.DOCKER_REST_PORT = 2375;
            DockerGlobal.DEBUG = true;
            DockerGlobal.DOCKER_REST_TIMEOUT = 100;

            CmdHubImageInfo cmdHubImageInfo =  CmdHubImageInfo.newInstance();
            cmdHubImageInfo.connect();
            Object data = cmdHubImageInfo.limit(10)
                    .term("centos")
                    .send();
            cmdHubImageInfo.close();
            Logger.info(formatJSON(data));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String formatJSON(Object obj){
        return JSON.formatJson(JSON.toJSON(obj));
    }
}
