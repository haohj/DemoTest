package com.hao.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

public class GsonTest {

    public static void main(String[] args) {
        /*Book book = new Book();
        book.setId("1");
        book.setName(null);
        book.setContent("hello");
        book.setCreate(new Date());
        Gson gson = new GsonBuilder().serializeNulls().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        System.out.println(gson.toJson(book));*/

        String sql = "select * from DB_CONFIG where INITIALSIZE = ${INITIALSIZE} and name = 'jakc' and MINIDLE= ${MINIDLE}";
        String where = sql.substring(sql.indexOf("where") + 5);
        System.out.println(where);

        String[] arr = where.split("=");
        for (int i = 0; i < arr.length; i++) {
            String field = arr[i];
            System.out.println(field);
            if (field.contains("?")) {
                field = field.replace("?", "").replace("and", "").replace("AND", "").trim();
                System.out.println(field);
            }
        }
    }
}
